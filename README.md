# CI/CD Challenge

1. Fix `Dockerfile`
2. Run the Docker image that was built
3. Push this image to a private Docker registry
4. Push image to registry only for master branch (bonus)